<?php
/**
 * @file
 * Responsive Image Sizes API
 * File manipulation functions for Responsive Image Sizes
 */

/**
 * Resize and save images.
 * 
 * @param $width
 *   Desired image width.
 * 
 * @param $height
 *   Desired image height.
 * 
 * @param $path
 *   Drupal path of source image.
 * 
 * @return
 *   Image path or FALSE if image can't be saved.
 */
function responsive_imgsize_resize($width, $height, $path) {
  $img = image_load($path);
  // TODO: drupal_realpath() is deprecated, and this may not work in some configurations.
  $base_directory = drupal_realpath(file_default_scheme() . '://') . '/responsive_imgsize';
  $base_path = preg_replace('/^' . preg_quote($_SERVER['DOCUMENT_ROOT'] . '/', '/') . '/', '', $base_directory);
  $scaled_path = $base_path . '/' . $width . '/' . $height;
  
  if (image_load($scaled_path . '/' . $path)) {
    return $scaled_path . '/' . $path;
  }
  
  file_prepare_directory(dirname($scaled_path . '/' . $path), FILE_CREATE_DIRECTORY);

  // TODO: Make upscaling configurable.
  image_scale($img, $width, $height, TRUE);
  if (image_save($img, $scaled_path . '/' . $path)) {
    return $scaled_path . '/' . $path;
  }
  
  return FALSE;
}

/**
 * Convert a URI into a drupal path.
 * 
 * @param $src
 *   A source URI.
 * 
 * @return
 *   Drupal path to image, or FALSE if it's not a valid image URI.
 */
function responsive_imgsize_uri_path($src) {
  // Determine if this is a local or remote file. Based on code in image_resize_filter.module.
  $base_path = base_path();
  $location = NULL;
  $path = NULL;
  if (strpos($src, $base_path) === 0) {
    $location = 'local';
    $path = preg_replace('/^' . preg_quote($base_path, '/') . '/', '', $src);
  }
  elseif (preg_match('/http[s]?:\/\/' . preg_quote($_SERVER['HTTP_HOST'] . $base_path, '/') . '/', $src)) {
    $location = 'local';
    $tmp = file_uri_target($src);
    $path = preg_replace('/^http[s]?:\/\/' . preg_quote($_SERVER['HTTP_HOST'] . $base_path, '/') . '/', '', $src);
  }
  elseif (strpos($src, 'http') === 0) {
    $location = 'remote';
  }
  
  // TODO: Save a local copy of remote file and return path to that copy.
  if ($location != 'local') {
    return FALSE;
  }
  
  return $path;
}
