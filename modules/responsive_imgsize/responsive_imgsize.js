(function ($) {
if (!Drupal.responsiveTools) {
  Drupal.responsiveTools = {};
}

Drupal.responsiveTools.responsiveImgsize = function(op) {
  if (op == 'resize') {
    // Don't execute on resize. It's way too demanding.
    return;
  }

  var settings = Drupal.settings.responsiveTools.responsiveImgsize;
  var setSrc = function(image) {
    return function(response, textStatus) {
      $('<img />')
      .attr('src', response)
      .load(function(){
        $(image).attr('src', response);
        $(image).fadeIn();
      });
    };
  };
  
  // Run for each selector in settings.
  for (i in settings) {
    $(i).each(function(index) {
      if (op == 'ready') {
        // Hide image until we can get correct src.
        $(this).hide();
      }
      else {
        var width = $(this).width();
        var height = $(this).height();
        $.get(Drupal.settings.basePath + 'responsive-imgsize/' + width + '/' + height + '/ajax', {'src' : $(this).attr('src'), 'selector' : i}, setSrc(this));
      }
    });
  }
}

})(jQuery);
