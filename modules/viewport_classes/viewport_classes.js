(function ($) {
if (!Drupal.responsiveTools) {
  Drupal.responsiveTools = {};
}

Drupal.responsiveTools.viewportClasses = function() {
  // Setup classes and maximum widths
  var classes = Drupal.settings.responsiveTools.viewportClasses;
  
  // Get current width
  var width = $(window).width();
  
  // Determine which class should be set for current width
  for (var i in classes) {
    if (((width >= classes[i]['min width']) || (classes[i]['min width'] == undefined))
     && ((width <= classes[i]['max width']) || (classes[i]['max width'] == undefined) || (classes[i]['max width'] == 0))) {
      $('body').addClass(i);
    }
    else {
      $('body').removeClass(i);
    }
  }
}

})(jQuery);
