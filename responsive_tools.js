(function ($) {
if (!Drupal.responsiveTools) {
  Drupal.responsiveTools = {};
}

Drupal.responsiveToolsExecute = function(op) {
  for (var i in Drupal.responsiveTools) {
    if ($('div#overlay').length) {
      // Don't execute in the context of an admin overlay.
      return;
    }
    Drupal.responsiveTools[i](op);
  }
}

Drupal.behaviors.responsiveTools = {
  attach: function(context, settings) {

    Drupal.responsiveToolsExecute('ready');
    $(window).load(
      function() {
        Drupal.responsiveToolsExecute('load');
      }
    );
    $(window).resize(
      function() {
        Drupal.responsiveToolsExecute('resize');
      }
    );
  }

}

})(jQuery);
